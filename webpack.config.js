var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  devtool: 'inline-source-map',
  entry: [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/index'
  ],
  output: {
    path: __dirname,
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.UglifyJsPlugin(),
//    new ExtractTextPlugin("index.css", {}),
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel?presets[]=es2015'],
      exclude: /node_modules/,
      include: __dirname
    },
//    {
//     test: /\.css$/,
//      loader: ExtractTextPlugin.extract("style-loader", "css-loader")
//    }
{
    test: /\.css$/,
    loader: 'style!css'
}
    ]
  }
};
