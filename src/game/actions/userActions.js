export const userActions = {
	USER_TURN: 'USER_TURN',
	HISTORY_NAVIGATE: 'HISTORY_NAVIGATE',
	ADMIT_DEFEAT: 'ADMIT_DEFEAT',
	DRAW_OFFER: 'DRAW_OFFER',
	DRAW_ACCEPT: 'DRAW_ACCEPT',
	TAKEBACK_OFFER: 'TAKEBACK_OFFER',
	TAKEBACK_ACCEPT: 'TAKEBACK_ACCEPT'
};