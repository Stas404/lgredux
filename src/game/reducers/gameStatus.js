// Import actions
import {userActions} from './../actions/userActions';

export function gameStatus (state = null , action) {
	switch (action.type) {

		case userActions.DRAW_OFFER:
			return {
				data: action.data,
				draw: 'offer'
			};

		case userActions.DRAW_ACCEPT:
			return {
				data: action.data,
				draw: 'accept'
			};

		default: return state;
	}
};             
