// Import actions
import {gameActions} from './../actions/gameActions';

export function gamePlayers (state = null , action) {
	switch (action.type) {

		case gameActions.GAME_TIMETICK:
			return {
				data: action.data,
				draw: 'time'
			};

		default: return state;
	}
};             