// Import actions
import {gameActions} from './../actions/gameActions';

export function gameHistory (state = {turns: [], activeTurn:0}, action) {
	switch (action.type) {

		case gameActions.GAME_TURN:
		//	return [...state, action.data];
			return {
				turns: [...state.turns, action.turn],
				currentTurn: action.activeTurn
			}

		default: return state;
	}
};