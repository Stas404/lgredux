// Import Redux
import {createStore, combineReducers} from 'redux';

// Import reducers
import {gameHistory} from './game/reducers/gameHistory.js';
import {gameStatus} from './game/reducers/gameStatus.js';
import {gamePlayers} from './game/reducers/gamePlayers.js';

// Import Actions
import {gameActions} from './game/actions/gameActions';
import {userActions} from './game/actions/userActions';


// Createing Store
let A =	combineReducers({
		gameStatus,
		gameHistory,
		gamePlayers
	});
let B =	combineReducers({
		gameStatus,
		gamePlayers
	})
window.store = createStore(
	combineReducers({
		A,
		B
	})
);


// State change (callback: log, update views, ...)
let unsubscribe = store.subscribe(() => {
	document.getElementById('debug').innerHTML = JSON.stringify(window.store.getState(), null, 4);
	console.log(store.getState());
});



// Demo usecase (array of actions)
let demoUsecase = require('./demoUsecase.js');
demoUsecase(1000);

require('./index.css');


// Stop listening changes of State
// unsubscribe();