var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');
/*
config.watch = true,
config.watchOptoins = {
		aggregateTimeout: 10,
		// poll: 100
		// опрос изменений
		// например, при сборке под виртуальной машиной, если не работает обычный watch
	}
*/
new WebpackDevServer(webpack(config), {
  hot: true,
  stats: {
    colors: true
  }
}).listen(3000, 'localhost', function (err) {
  if (err) {
    console.log(err);
  }

  console.log('Listening at localhost:3000');
});
